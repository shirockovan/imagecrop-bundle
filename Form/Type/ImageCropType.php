<?php
namespace Artsofte\ImageCropBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ImageCropType extends AbstractType
{
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setOptional(array('object'));
    $resolver->setDefaults(array(
      'type' => 'hidden',
    ));
  }

  public function buildView(FormView $view, FormInterface $form, array $options){
    $view->vars['isset_image'] = $options['object'] ? $options['object']->issetImage() : false ; //существует ли файл изображения
    $view->vars['image_path'] = $options['object'] ? $options['object']->getImagePath() : false ; //путь до изображения
    $view->vars['image_width'] = $options['object'] ? $options['object']->getImageWidthOriginal() : '' ; //ширина исходного изображения
    $view->vars['image_height'] = $options['object'] ? $options['object']->getImageHeightOriginal() : '' ; //высота исходного изображения
    list($view->vars['crop_width'], $view->vars['crop_height']) = $options['object'] ? $options['object']->getCropSize(0) : array(false,false); //ширина и высота кропа
  }


  public function getParent()
  {
    return 'hidden';
  }

  public function getName()
  {
    return 'image_crop';
  }
}