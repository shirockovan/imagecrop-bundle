ArtsofteImageCropBundle
============

Introduction
------------
Component allows you to crop's images.

Documentation
-------------

For more information, see the documentation: [RU](http://gitlab.artsofte.ru/bundles/imagecrop-bundle/blob/master/Resources/doc/ru.md) [EN](http://gitlab.artsofte.ru/bundles/imagecrop-bundle/blob/master/Resources/doc/en.md).