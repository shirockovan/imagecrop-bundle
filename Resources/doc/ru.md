ArtsofteImageCropBundle
==================================

Компонент предназначен для кропа изображений. Кол-во кропов у изображения может быть любым

## Установка

1. Скачать ArtsofteImageCropBundle используя композер
2. Подключить бандл
3. Настроить в config.yml

### Шаг 1: Скачать ArtsofteImageCropBundle используя композер

Добавьте ArtsofteImageCropBundle в composer.json:

```js
{
    "require": {
        "artsofte/imagecrop-bundle": "dev-master"
	},
    "repositories": [
        { "type": "git", "url": "git@gitlab.artsofte.ru:bundles/imagecrop-bundle.git" }
    ],
}
```

Теперь запустите композер, чтобы скачать бандл командой:

``` bash
$ composer update artsofte/imagecrop-bundle
```

Композер установит бандл в папку проекта `vendor/artsofte/imagecrop-bundle`.

### Шаг 2: Подключить бандл

Подключите бандл в ядре `AppKernel.php`:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Artsofte\ImageCropBundle\ArtsofteImageCropBundle(),
    );
}
```

### Шаг 3: Настроить в config.yml

Для того, чтобы использовать бихейвор image_crop, его нужно объявить в файле `config.yml`.

``` yaml
# app/config/config.yml
propel:
    ...
    behaviors:
        artsofte_image_crop: Artsofte\ImageCropBundle\Behavior\ArtsofteImageCropBehavior
```


Использование бихейвора image_crop
-----
Для того, чтобы воспользоваться бихейвором, достаточно у нужной таблицы в файле `schema.xml` добавить тег `<behavior name="artsofte_image_crop" />`. Так же можно воспользоваться расширенной записью тега бихейвора, с указанием параметров:
- `image_column` название столбца имени файла изображения. По-умолчанию `image`. Тип столбца `varchar`.
- `coords_column` название столбца координат кропа изображения. По-умолчанию `coords`. Тип столбца `varchar`.
- `crop_size` размеры кропов изображения. Можно указывать несколько размеров через запятую.

``` xml
<behavior name="artsofte_image_crop">
  <parameter name="image_column"  value="image" />
  <parameter name="coords_column" value="coords" />
  <parameter name="crop_size"     value="980x380,134x52" />
</behavior>
```

Примеры
-------

``` yml
<table name="banner" phpName="Banner">
  <column name="id"     phpName="Id"      type="BIGINT" primaryKey="true" autoIncrement="true" required="true" />
  <column name="active" phpName="Active"  type="BOOLEAN" required="false" />      
  <column name="title"  phpName="Title"   type="VARCHAR" required="true" />
  <column name="href"   phpName="Href"    type="VARCHAR" required="true" />
  <column name="body"   phpName="Body"    type="longvarchar" required="false" />

  <behavior name="artsofte_image_crop">
    <parameter name="crop_size" value="500x200" />
  </behavior>
</table>

<table name="teaser" phpName="Teaser">
    <column name="id"     phpName="Id"      type="BIGINT" primaryKey="true" autoIncrement="true" required="true" />
    <column name="title"  phpName="Title"   type="VARCHAR" required="true" />
    <column name="image"  phpName="Image"   type="VARCHAR" required="false" />
    <column name="coords" phpName="Coords"  type="OBJECT" required="false" />

    <behavior name="artsofte_image_crop">
      <parameter name="image_column"  value="image" />
      <parameter name="coords_column" value="coords" />
      <parameter name="crop_size"     value="980x380,134x52" />
    </behavior>
</table>
```

С помощью бихейвора, в базовый класс баннера будут добавлены методы для работы с кропами изображения. Например метод сохранения кропа будет выглядеть так:

``` php
<?php
// ...\Model\om\BasBanner.php;

 ...

    // artsofte_image_crop behavior

    /**
     * Сохранение кропа изображения
     */
    public function saveThumbs(){
      $coords = $this->getCoords();
      $file_image = $this->getImageFile();
      $ext = $file_image->getExtension();
      $name = substr($file_image->getFilename(), 0, -(strlen($ext)+1) );
      $ext == "jpeg" ? $ext = "jpg" : $ext;
      $src = $this->getFullImagePath();

      list($width,$height) = $this->getCropSize();
      $x1 = $coords["x1"];
      $y1 = $coords["y1"];
      $x1_to = 0;
      $y1_to = 0;

      foreach($this->getCropSizeArray() as $num => $size ){
        $file_thumb_name = $name."_thumb".($num?$num:"").".".$ext;
        $file_thumb_path = $this->getProjectPath().$this->getImagePathDir().$file_thumb_name;
        list($width_to, $height_to) = $this->getCropSize($num);
        $image_to = imagecreatetruecolor($width_to, $height_to);

        switch($ext){
          case "bmp": $image = imagecreatefromwbmp($src); break;
          case "gif": $image = imagecreatefromgif($src); break;
          case "jpg": $image = imagecreatefromjpeg($src); break;
          case "png": $image = imagecreatefrompng($src); break;
          default : return "Unsupported picture type!";
        }
        if($ext == "gif" || $ext == "png"){
          imagecolortransparent($image_to, imagecolorallocatealpha($image_to, 0, 0, 0, 127));
          imagealphablending($image_to, false);
          imagesavealpha($image_to, true);
        }
        imagecopyresampled($image_to, $image, $x1_to, $y1_to, $x1, $y1, $width_to, $height_to, $width, $height);
        switch($ext){
          case "bmp": imagewbmp($image_to, $file_thumb_path); break;
          case "gif": imagegif($image_to, $file_thumb_path); break;
          case "jpg": imagejpeg($image_to, $file_thumb_path); break;
          case "png": imagepng($image_to, $file_thumb_path); break;
        }
      }
    }
    ...
}
```

Использование виджета image_crop_widget в форме
-----

#### Шаг 1. Добавьте 2 поля `image_file` и `image_crop_coords` в методы `->add()` формы. Переименовывать эти поля нельзя.
``` php
<?php
// src/Your/NameBundle/Form/YourType.php

...
      ->add('image_file','file', array(
        'label'       => 'Изображение',
        'required'    => false,
        'constraints' => array(
          new Image(array(
            'minWidth' => Teaser::getCropSizeWidth(),
            'minHeight' => Teaser::getCropSizeHeight(),
            'mimeTypes' => array(
              'image/gif',
              'image/jpeg',
              'image/pjpeg',
              'image/png'
            )
          ))
        )
      ))
      ->add('image_crop_coords', 'image_crop', array(
        'attr' => array(
          'class' => 'image_crop_coords',
        ),
        'object' => $object(,
      ))
```
#### Шаг 2. Добавьте в шаблоне, где будет выводиться форма, подключение js и css файлов бандла ArtsofteImageCropBundle:
``` twig
{% block stylesheets %}
  {{ parent() }}
  {{ include('ArtsofteImageCropBundle:Form:css.html.twig') }}
{% endblock %}

{% block javascripts %}
  {{ parent() }}
  {{ include('ArtsofteImageCropBundle:Form:js.html.twig') }}
{% endblock %}
```

### Пример

Форма создания тизера:

``` php
<?php
// .../Form/Teaser/TeaserType.php;
class TeaserType extends BaseAbstractType
{
  protected $options = array(
    'data_class'  => 'Artsofte\MainBundle\Model\Teaser',
    'name'        => 'teaser',
  );

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('title', 'text', array(
        'label'       => 'Название',
        'required'    => TRUE,
        'constraints' => array(
          new NotBlank(array(
            'message' => 'Укажите название'
          ))
        )
      ))
      ->add('image_file','file', array(
        'label'       => 'Изображение',
        'required'    => false,
        'constraints' => array(
          new Image(array(
            'minWidth' => Teaser::getCropSizeWidth(),
            'minHeight' => Teaser::getCropSizeHeight(),
            'mimeTypes' => array(
              'image/gif',
              'image/jpeg',
              'image/pjpeg',
              'image/png'
            )
          ))
        )
      ))
      ->add('image_crop_coords', 'image_crop', array(
        'attr' => array(
          'class' => 'image_crop_coords',
        ),
        'object' => $this->getSubject(,
      ));
  }

  public function getName(){
    return 'teaser';
  }

  public function issetImage(){
    return true;
  }
}
```

### Путь сохранения кропов изображения
Файлы кропов, вместе с оригиналом изображения, будут сохраняться в папку `web/uploads/model_name/id/`

## Вывод изображения

Для вывода изображения, используется метод `getThumb`, сформированный бихейвором. В качестве параметра этому методу передаётся порядковый номер кропа.

``` twig
<!-- без номера кропа -->
<img src="{{ teaser.thumb }}" />
```

``` twig
<!-- с указанием номера кропа -->
<img src="{{ teaser.getThumb(1) }}" />
```