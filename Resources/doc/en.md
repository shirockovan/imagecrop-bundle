Getting Started With ArtsofteImageCropBundle
==================================

Component allows you to crop's images.

## Installation

1. Download ArtsofteImageCropBundle using composer
2. Enable the Bundle
3. Configure your application's config.yml

### Step 1: Download ArtsofteImageCropBundle using composer

Add ArtsofteImageCropBundle in your composer.json:

```js
{
    "require": {
        "artsofte/imagecrop-bundle": "dev-master"
	},
    "repositories": [
        { "type": "git", "url": "git@gitlab.artsofte.ru:bundles/imagecrop-bundle.git" }
    ],
}
```

Now tell composer to download the bundle by running the command:

``` bash
$ composer update artsofte/imagecrop-bundle
```

Composer will install the bundle to your project's `vendor/artsofte/imagecrop-bundle` directory.

### Step 2: Enable the bundle

Enable the bundle in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Artsofte\ImageCropBundle\ArtsofteImageCropBundle(),
    );
}
```

### Step 3: Configure your application's config.yml

In order for use image_crop behavior, you must tell it to do so in the `config.yml` file.

``` yaml
# app/config/config.yml
propel:
    ...
    behaviors:
        artsofte_image_crop: Artsofte\ImageCropBundle\Behavior\ArtsofteImageCropBehavior
```


Usage image_crop behavior
-----

Just add the following tag in your `schema.xml` file:

``` xml
<behavior name="artsofte_image_crop">
  <parameter name="image_column"  value="image" />
  <parameter name="coords_column" value="coords" />
  <parameter name="crop_size"     value="980x380,134x52" />
</behavior>
```

Example
-------

``` yml
<table name="banner" phpName="Banner">
  <column name="id"     phpName="Id"      type="BIGINT" primaryKey="true" autoIncrement="true" required="true" />
  <column name="active" phpName="Active"  type="BOOLEAN" required="false" />      
  <column name="title"  phpName="Title"   type="VARCHAR" required="true" />
  <column name="href"   phpName="Href"    type="VARCHAR" required="true" />
  <column name="body"   phpName="Body"    type="longvarchar" required="false" />

  <behavior name="artsofte_image_crop">
    <parameter name="crop_size" value="500x200" />
  </behavior>
</table>

<table name="teaser" phpName="Teaser">
    <column name="id"     phpName="Id"      type="BIGINT" primaryKey="true" autoIncrement="true" required="true" />
    <column name="title"  phpName="Title"   type="VARCHAR" required="true" />
    <column name="image"  phpName="Image"   type="VARCHAR" required="false" />
    <column name="coords" phpName="Coords"  type="OBJECT" required="false" />

    <behavior name="artsofte_image_crop">
      <parameter name="image_column"  value="image" />
      <parameter name="coords_column" value="coords" />
      <parameter name="crop_size"     value="980x380,134x52" />
    </behavior>
</table>
```

It will generate the following code in the `BaseBanner` class:

``` php
<?php
// ...\Model\om\BasBanner.php;

 ...

    // artsofte_image_crop behavior

    /**
     * Сохранение кропа изображения
     */
    public function saveThumbs(){
      $coords = $this->getCoords();
      $file_image = $this->getImageFile();
      $ext = $file_image->getExtension();
      $name = substr($file_image->getFilename(), 0, -(strlen($ext)+1) );
      $ext == "jpeg" ? $ext = "jpg" : $ext;
      $src = $this->getFullImagePath();

      list($width,$height) = $this->getCropSize();
      $x1 = $coords["x1"];
      $y1 = $coords["y1"];
      $x1_to = 0;
      $y1_to = 0;

      foreach($this->getCropSizeArray() as $num => $size ){
        $file_thumb_name = $name."_thumb".($num?$num:"").".".$ext;
        $file_thumb_path = $this->getProjectPath().$this->getImagePathDir().$file_thumb_name;
        list($width_to, $height_to) = $this->getCropSize($num);
        $image_to = imagecreatetruecolor($width_to, $height_to);

        switch($ext){
          case "bmp": $image = imagecreatefromwbmp($src); break;
          case "gif": $image = imagecreatefromgif($src); break;
          case "jpg": $image = imagecreatefromjpeg($src); break;
          case "png": $image = imagecreatefrompng($src); break;
          default : return "Unsupported picture type!";
        }
        if($ext == "gif" || $ext == "png"){
          imagecolortransparent($image_to, imagecolorallocatealpha($image_to, 0, 0, 0, 127));
          imagealphablending($image_to, false);
          imagesavealpha($image_to, true);
        }
        imagecopyresampled($image_to, $image, $x1_to, $y1_to, $x1, $y1, $width_to, $height_to, $width, $height);
        switch($ext){
          case "bmp": imagewbmp($image_to, $file_thumb_path); break;
          case "gif": imagegif($image_to, $file_thumb_path); break;
          case "jpg": imagejpeg($image_to, $file_thumb_path); break;
          case "png": imagepng($image_to, $file_thumb_path); break;
        }
      }
    }
    ...
}
```

Usage widget image_crop_widget in your form
-----

#### Step 1. Add the following `->add()` methods in your form file:
``` php
      ->add('image_file','file', array(
        'label'       => 'Изображение',
        'required'    => false,
        'constraints' => array(
          new Image(array(
            'minWidth' => Teaser::getCropSizeWidth(),
            'minHeight' => Teaser::getCropSizeHeight(),
            'mimeTypes' => array(
              'image/gif',
              'image/jpeg',
              'image/pjpeg',
              'image/png'
            )
          ))
        )
      ))
      ->add('image_crop_coords', 'image_crop', array(
        'attr' => array(
          'class' => 'image_crop_coords',
        ),
        'object' => $this->getSubject(),
      ))
```
#### Step 2. Add assets bundle in template:
``` twig
{% block stylesheets %}
  {{ parent() }}
  {{ include('ArtsofteImageCropBundle:Form:css.html.twig') }}
{% endblock %}

{% block javascripts %}
  {{ parent() }}
  {{ include('ArtsofteImageCropBundle:Form:js.html.twig') }}
{% endblock %}
```

Example
-------
``` php
<?php
// ...\Form\Teaser\TeaserType.php;
class TeaserType extends BaseAbstractType
{
  protected $options = array(
    'data_class'  => 'Artsofte\MainBundle\Model\Teaser',
    'name'        => 'teaser',
  );

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $object = $builder->getData();

    $builder
      ->add('title', 'text', array(
        'label'       => 'Название',
        'required'    => TRUE,
        'constraints' => array(
          new NotBlank(array(
            'message' => 'Укажите название'
          ))
        )
      ))
      ->add('image_file','file', array(
        'label'       => 'Изображение',
        'required'    => false,
        'constraints' => array(
          new Image(array(
            'minWidth' => Teaser::getCropSizeWidth(),
            'minHeight' => Teaser::getCropSizeHeight(),
            'mimeTypes' => array(
              'image/gif',
              'image/jpeg',
              'image/pjpeg',
              'image/png'
            )
          ))
        )
      ))
      ->add('image_crop_coords', 'image_crop', array(
        'attr' => array(
          'class' => 'image_crop_coords',
        ),
        'object' => $object,
      ));
  }

  public function getName(){
    return 'teaser';
  }

  public function issetImage(){
    return true;
  }
}
```

### Crop's image storage path
Crop's files, together with the original image will be saved in the folder `web/uploads/model_name/id/`

## Display an image in a browser

To display an image, the method of `getThumb`, formed behavior. The parameter passed to this method crop serial number.

``` twig
<!-- no serial number -->
<img src="{{ teaser.thumb }}" />
```

``` twig
<!-- with serial number -->
<img src="{{ teaser.getThumb(1) }}" />
```