var ratio = 1;

function coords_set(c) {

    var x1 = ~~(c.x/ratio);
    var y1 = ~~(c.y/ratio);
    var x2 = ~~(c.x2/ratio);
    var y2 = ~~(c.y2/ratio);
    var coords = x1+';'+y1+';'+x2+';'+y2;

    $('.image_crop_coords').val(coords);
}

function image_crop_load() {
    var image_crop = $('.image_crop:visible');
    if (!image_crop.length) {
        return setTimeout(image_crop_load, 1000);
    }

    ratio = $(image_crop).width()/$('#image_crop_image_width').val();

    var crop_width = ~~($('#image_crop_width').val()*ratio);
    var crop_height = ~~($('#image_crop_height').val()*ratio);

    var coords = $('.image_crop_coords').val().split(';');

    $(image_crop).Jcrop({
        onChange:   coords_set,
        onSelect:   coords_set,
        minSize: [crop_width, crop_height],
        aspectRatio: crop_width / crop_height
    },function(){
        jcrop_api = this;

        if(coords.length>1){
            coords.forEach(function(val,i) {
                coords[i]=~~(val*ratio);
            });

            jcrop_api.setSelect(coords);
        }
    });
}

function image_crop_delete(e) {
    e.preventDefault();
    $('.image_crop, .jcrop-holder, #image_crop_delete').hide();
    $('.image_crop_coords').val('delete');
}

$(function() {
    if ($('.image_crop').length) {
        image_crop_load();
        $(document).on('click', '#image_crop_delete', image_crop_delete);
    }
});