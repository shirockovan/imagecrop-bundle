<?php
/**
 * Created by PhpStorm.
 * User: yakimov
 * Date: 27.05.14
 * Time: 18:05
 */
namespace Artsofte\ImageCropBundle\Behavior;

class ArtsofteImageCropBehavior extends \Behavior
{
    // default parameters value
    protected $parameters = array(
        'image_column'  => 'image',
        'coords_column' => 'coords',
        'crop_size'     => '60x60',
    );

    /**
     * Проверяем и добавляем столбцы image,coords при их отсутствии
     *
     * @throws InvalidArgumentException
     */
    public function modifyTable()
    {
        $table = $this->getTable();

        //IMAGE
        if (!$columnName = $this->getParameter('image_column')) {
            throw new InvalidArgumentException(sprintf(
                'You must define a \'image\' parameter for the \'aggregate_column\' behavior in the \'%s\' table',
                $table->getName()
            ));
        }
        // add the aggregate column if not present
        if(!$table->hasColumn($columnName)) {
            $table->addColumn(array(
                'name'    => $columnName,
                'type'    => 'VARCHAR',
            ));
        }

        //COORDS
        if (!$columnName = $this->getParameter('coords_column')) {
            throw new InvalidArgumentException(sprintf(
                'You must define a \'coords\' parameter for the \'aggregate_column\' behavior in the \'%s\' table',
                $table->getName()
            ));
        }
        // add the aggregate column if not present
        if(!$table->hasColumn($columnName)) {
            $table->addColumn(array(
                'name'    => $columnName,
                'type'    => 'OBJECT',
            ));
        }
    }

    /**
     * Add behavior attributes to model class
     *
     * @return string The code to be added to model class
     */
    public function objectAttributes()
    {
        $crops = explode(',', $this->getParameter('crop_size'));

        $attributes = '
protected $fileFolder;
protected $fileName;
protected $file;
protected $delete_crop_image = false;
protected $old_image;
protected $old_thumbs = array();
static protected $crop_size = array(
';
        foreach($crops as $i=>$crop)
            $attributes.=$i.'=>"'.$crop.'",
';
        $attributes.=');';
        return $attributes;
    }

    /**
     * Список методов
     *
     * @param $builder
     * @return string
     */
    public function objectMethods($builder)
    {
        $this->builder = $builder;
        $script = '';
        $this->getProjectPath($script);         //Путь до проекта на сервере
        $this->getImageFile($script);           //Возвращает файл изображения
        $this->setImageFile($script);           //Установка файла изображения
        $this->getCropSize($script);            //Размеры кропа
        $this->setImageCropCoords($script);     //Координаты кропа
        $this->getCropSizeWidth($script);       //Размеры кропа. Ширина
        $this->getCropSizeHeight($script);      //Размеры кропа. Высота
        $this->getCropSizeArray($script);       //Массив с размерами кропов
        $this->getImageCropCoords($script);     //Координаты кропа
        $this->getImagePathName($script);       //Имя класса в нижнем регистре
        $this->getImagePathDir($script);        //Путь до папки, где лежит файл изображения
        $this->getImagePath($script);           //Путь до файла изображения в рамках проекта
        $this->fullImagePath($script);          //Полный путь файла изображения на сервере
        $this->getFullImagePath($script);       //Полный путь файла изображения на сервере. Если файла не существует, возвращается FALSE
        $this->issetImage($script);             //Проверка существования файла изображения на сервере
        $this->getImageSizeOriginal($script);   //Оригинальные размеры изображения
        $this->getImageWidthOriginal($script);  //Ширина оригинального изображения
        $this->getImageHeightOriginal($script); //Высота оригинального изображения
        $this->saveThumbs($script);             //Сохранение кропа изображения
        $this->getThumb($script);               //Путь до файла кропа
        $this->getThumbName($script);           //Имя файла кропа
        $this->clearImageFolder($script);       //удаляем все файлы из папки, куда будем заливать новое изображение
        $this->deleteCropFiles($script);        //Удаляем файлы изображений (кропов)

        return $script;
    }

    /**
     * Возвращает название метода вызова изображения (getImage/setImage)
     *
     * @param string $method set/get
     * @return string 'getImage'
     */
    protected function getImageColumn($method = 'set')
    {
        return $method . $this->getColumnForParameter('image_column')->getPhpName();
    }

    /**
     * * Возвращает название метода вызова координат (getCoords/setCoords)
     *
     * @param string $method set/get
     * @return string
     */
    protected function getCoordsColumn($method = 'set')
    {
        return $method . $this->getColumnForParameter('coords_column')->getPhpName();
    }

    /**
     * Возвращает файл изображения
     *
     * @param $script
     */
    protected function getImageFile(&$script){
        $script .= '
/**
 * Возвращает файл изображения
 *
 * @return File
 */
public function getImageFile(){
    return new \Symfony\Component\HttpFoundation\File\File($this->getImagePath(), FALSE);
}
    ';
    }

    /**
     * Установка файла изображения
     *
     * @param $script
     */
    protected function setImageFile(&$script){
        $script .= '
/**
 * Установка файла изображения
 *
 * @param UploadedFile $file
 */
public function setImageFile(\Symfony\Component\HttpFoundation\File\UploadedFile $file = NULL)
{
    if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
        $this->file       = $file;
        $this->fileName   = uniqid() .".". strtolower($this->file->getClientOriginalExtension());
        $this->old_image = $this->getImage();
        if ($this->old_image) {
            $crop_sizes = $this->getCropSizeArray();
            foreach($crop_sizes as $num => $size) {
                $this->old_thumbs[] = $this->getThumbName($num);
            }
        }
        $this->'.$this->getImageColumn('set').'($this->fileName);
    }
}
    ';
    }

    /**
     * Размеры кропа
     *
     * @param $script
     */
    protected function getCropSize(&$script){
        $script .= '
/**
 * Размеры кропа
 *
 * @param $num Номер кропа
 * @return array (width, height)
 */
public function getCropSize($num=0){
  return isset(self::$crop_size[$num]) ? explode("x",self::$crop_size[$num]) : array(false,false);
}
    ';
    }

    /**
     * Координаты кропа
     *
     * @param $script
     */
    protected function setImageCropCoords(&$script){
        $script .= '
/**
 * Координаты кропа
 *
 * @param $v $x1;$y1;$x2;$y2
 */
public function setImageCropCoords($v){
  if($v && $v!="delete"){
    list($x1,$y1,$x2,$y2) = explode(";",$v); //присланные координаты
    $coords = array(
      "x1" => $x1,
      "y1" => $y1,
      "x2" => $x2,
      "y2" => $y2,
    );
  }
  else{
    list($width,$height) = $this->getCropSize();
    $coords = array(
      "x1" => 0,
      "y1" => 0,
      "x2" => $width,
      "y2" => $height,
    );
      if($v=="delete") {
          $this->delete_crop_image = true;
      }
  }
  $this->'.$this->getCoordsColumn('set').'($coords);
}
    ';
    }

    /**
     * Координаты кропа
     *
     * @param $script
     */
    protected function getImageCropCoords(&$script){
        $script .= '
/**
 * Координаты кропа
 *
 * @return string
 */
public function getImageCropCoords(){
  return implode(";",$this->'.$this->getCoordsColumn('get').'() ? $this->'.$this->getCoordsColumn('get').'() : array());
}
    ';
    }

    /**
     * Размеры кропа. Ширина
     *
     * @param $script
     */
    protected function getCropSizeWidth(&$script){
        $script .= '
/**
 * Размеры кропа. Ширина
 *
 * @param $num Номер кропа
 * @return string
 */
static public function getCropSizeWidth($num=0){
  list($width, $height) = explode("x",self::$crop_size[$num]);
  return $width;
}
    ';
    }

    /**
     * Размеры кропа. Высота
     *
     * @param $script
     */
    protected function getCropSizeHeight(&$script){
        $script .= '
/**
 * Размеры кропа. Высота
 *
 * @param $num Номер кропа
 * @return string
 */
static public function getCropSizeHeight($num=0){
  list($width, $height) = explode("x",self::$crop_size[$num]);
  return $height;
}
    ';
    }

    /**
     * Массив с размерами кропов
     *
     * @param $script
     */
    protected function getCropSizeArray(&$script){
        $script .= '
/**
 * Массив с размерами кропов
 *
 * @return array
 */
public function getCropSizeArray(){
  return self::$crop_size;
}
    ';
    }

    /**
     * Путь до проекта на сервере
     *
     * @param $script
     */
    protected function getProjectPath(&$script){
        $script .= '
/**
 * Путь до проекта на сервере
 *
 * @return string
 */
public function getProjectPath(){
  return $_SERVER["DOCUMENT_ROOT"];
}
    ';
    }

    /**
     * Имя класса в нижнем регистре
     *
     * @param $script
     */
    protected function getImagePathName(&$script){
        $script .= '
/**
 * Имя класса в нижнем регистре
 *
 * @return string
 */
public function getImagePathName(){
  return join("", array_slice(explode("\\\\", strtolower(get_class($this)) ), -1));
}
    ';
    }

    /**
     * Путь до папки, где лежит файл изображения
     *
     * @param $script
     */
    protected function getImagePathDir(&$script){
        $script .= '
/**
 * Путь до папки, где лежит файл изображения
 *
 * @return string
 */
public function getImagePathDir(){
  return "/uploads/".$this->getImagePathName()."/".$this->getId()."/";
}
    ';
    }

    /**
     * Путь до файла изображения в рамках проекта
     *
     * @param $script
     */
    protected function getImagePath(&$script){
        $script .= '
/**
 * Путь до файла изображения в рамках проекта
 *
 * @return string
 */
public function getImagePath(){
  return $this->getImagePathDir().$this->'.$this->getImageColumn('get').'();
}
    ';
    }

    /**
     * Полный путь файла изображения на сервере
     *
     * @param $script
     */
    protected function fullImagePath(&$script){
        $script .= '
/**
 * Полный путь файла изображения на сервере
 *
 * @return string
 */
protected function fullImagePath(){
  return $this->getProjectPath().$this->getImagePath();
}
    ';
    }

    /**
     * Полный путь файла изображения на сервере
     * Если файла не существует, возвращается FALSE
     *
     * @param $script
     */
    protected function getFullImagePath(&$script){
        $script .= '
/**
 * Полный путь файла изображения на сервере
 * Если файла не существует, возвращается FALSE
 *
 * @return string
 */
public function getFullImagePath(){
  return $this->issetImage() ? $this->fullImagePath() : false;
}
    ';
    }

    /**
     * Проверка существования файла изображения на сервере
     *
     * @param $script
     */
    protected function issetImage(&$script){
        $script .= '
/**
 * Проверка существования файла изображения на сервере
 *
 * @return bool
 */
public function issetImage(){
  return ($this->getId() && $this->'.$this->getImageColumn('get').'()) ? file_exists($this->fullImagePath()) : false;
}
    ';
    }

    /**
     * Оригинальные размеры изображения
     *
     * @param $script
     */
    protected function getImageSizeOriginal(&$script){
        $script .= '
/**
 * Оригинальные размеры изображения
 *
 * @return array (width,$height)
 */
public function getImageSizeOriginal(){
  return $this->issetImage() ? getimagesize($this->getFullImagePath()) : array(false,false);
}
    ';
    }

    /**
     * Ширина оригинального изображения
     *
     * @param $script
     */
    protected function getImageWidthOriginal(&$script){
        $script .= '
/**
 * Ширина оригинального изображения
 *
 * @return string
 */
public function getImageWidthOriginal(){
  list($width, $height) = $this->getImageSizeOriginal();
  return $width;
}
    ';
    }

    /**
     * Высота оригинального изображения
     *
     * @param $script
     */
    protected function getImageHeightOriginal(&$script){
        $script .= '
/**
 * Высота оригинального изображения
 *
 * @return string
 */
public function getImageHeightOriginal(){
  list($width, $height) = $this->getImageSizeOriginal();
  return $height;
}
    ';
    }

    /**
     * Путь до файла кропа
     *
     * @param $script
     */
    protected function getThumb(&$script){
        $script .= '
/**
 * Путь до файла кропа
 *
 * @param int $num
 * @return string
 */
public function getThumb($num=0){
  return $this->getImagePathDir().$this->getThumbName($num);
}
    ';
    }

    /**
     * Имя файла кропа
     *
     * @param $script
     */
    protected function getThumbName(&$script){
        $script .= '
  /**
   * Имя файла кропа
   *
   * @param int $num
   * @return string
   */
  public function getThumbName($num=0){
    $file_image = $this->getImageFile();
    $ext = $file_image->getExtension();
    $name = substr($file_image->getFilename(), 0, -(strlen($ext)+1) );
    return $name."_thumb".($num?$num:"").".".$ext;
  }
    ';
    }

    /**
     * удаляем все файлы из папки, куда будем заливать новое изображение
     *
     * @param $script
     */
    protected function clearImageFolder(&$script){
        $script .= '
/**
 * удаляем все файлы из папки, куда будем заливать новое изображение
 *
 * @return string
 */
public function clearImageFolder(){
    $folder = $this->getProjectPath().$this->getImagePathDir();

    //удаляем вначале оригинал, потом кропы
    $original_image = $this->old_image ? $this->old_image : $this->getImage();
    $file = $folder.$original_image;
    if (file_exists($file)) {
        unlink($file);
    }

    //теперь файлы кропов
    if (count($this->old_thumbs)) {
        foreach($this->old_thumbs as $thumb) {
            $file = $folder . $thumb;
            if (file_exists($file)) {
                unlink($file);
            }
        }
        $this->old_thumbs = array();
    } else {
        $crop_sizes = $this->getCropSizeArray();
        foreach ($crop_sizes as $num => $size) {
            $file = $folder.$this->getThumbName($num);
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }
}
    ';
    }

    /**
     * Сохранения кропа изображения
     *
     * @param $script
     */
    protected function saveThumbs(&$script){
        $script .= '
/**
 * Сохранение кропа изображения
 */
public function saveThumbs(){
    if ($this->old_image) {
        list($width,$height) = $this->getCropSize();
        $coords = array(
            "x1" => 0,
            "y1" => 0,
            "x2" => $width,
            "y2" => $height,
        );
        $this->setCoords($coords);
    }
    if ($this->delete_crop_image) {
        $this->deleteCropFiles();
    }
    if ($this->getFullImagePath()) {
      $coords = $this->'.$this->getCoordsColumn('get').'();
      $file_image = $this->getImageFile();
      $ext = $file_image->getExtension();
      $name = substr($file_image->getFilename(), 0, -(strlen($ext)+1) );
      $ext == "jpeg" ? $ext = "jpg" : $ext;
      $src = $this->getFullImagePath();
      $size = getimagesize($src);

      $width = $coords["x2"] - $coords["x1"];
      $height = $coords["y2"] - $coords["y1"];
      
      if ($coords["x2"] < $size[0] && $coords["y2"] < $size[1]) {
         $width += 1;
         $height += 1;
      }
      
      $x1 = $coords["x1"];
      $y1 = $coords["y1"];
      $x1_to = 0;
      $y1_to = 0;

      foreach($this->getCropSizeArray() as $num => $size ){
        $file_thumb_path = $this->getProjectPath().$this->getImagePathDir().$this->getThumbName($num);
        list($width_to, $height_to) = $this->getCropSize($num);
        $image_to = imagecreatetruecolor($width_to, $height_to);

        switch($ext){
          case "bmp": $image = imagecreatefromwbmp($src); break;
          case "gif": $image = imagecreatefromgif($src); break;
          case "jpg": $image = imagecreatefromjpeg($src); break;
          case "png": $image = imagecreatefrompng($src); break;
          default : return "Unsupported picture type!";
        }
        if($ext == "gif" || $ext == "png"){
          imagecolortransparent($image_to, imagecolorallocatealpha($image_to, 0, 0, 0, 127));
          imagealphablending($image_to, false);
          imagesavealpha($image_to, true);
        }

        imagecopyresampled($image_to, $image, $x1_to, $y1_to, $x1, $y1, $width_to, $height_to, $width, $height);
        switch($ext){
          case "bmp": imagewbmp($image_to, $file_thumb_path); break;
          case "gif": imagegif($image_to, $file_thumb_path); break;
          case "jpg": imagejpeg($image_to, $file_thumb_path, 95); break;
          case "png": imagepng($image_to, $file_thumb_path); break;
        }
      }
    }
    if ($this->old_image) {
        $this->file = false;
        $this->old_image = false;
        $this->save();
    }
}
    ';
    }

    /**
     * postSave. Сохраняем файл, делаем кропы
     *
     * @return string
     */
    public function postSave(){
        return '
if ($this->file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
  $this->clearImageFolder(); //удаляем все файлы из папки, куда будем заливать новое изображение
  $this->file->move($this->getProjectPath() . $this->getImagePathDir(), $this->fileName);
  $this->file = false;
}

//сохраняем кропы
if ($this->issetImage()) {
    $this->saveThumbs();
}
if ($this->delete_crop_image) {
    $this->deleteCropFiles();
}
    ';
    }

    /**
     * Удаляем файлы перед удалением объекта
     *
     * @param $builder
     * @return string
     */
    public function preDelete($builder)
    {
        $this->builder = $builder;
        $script = "
\$this->deleteCropFiles(); //Перед удалением объекта удаляем загруженные файлы";
        return $script;
    }

    /**
     * Удаляем файлы
     *
     * @param $script
     */
    protected function deleteCropFiles(&$script)
    {
        $script .= '
/**
 * Удаляем файлы изображений (кропов)
 *
 * @return bool
 */
public function deleteCropFiles()
{
    $this->clearImageFolder(); //вначале удаляем файлы кропов
    $files_dir = $this->getProjectPath().$this->getImagePathDir();
    $files = glob($files_dir."*.*");
    if (is_dir($files_dir) && !count($files)) { //если в папке есть ещё чьи то файлы, то папку не трогаем. Если пустая, то удаляем
        return rmdir($files_dir);
    }
    $this->delete_crop_image = false;
    return true;
}
        ';
    }

}